import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../product/product.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  productForm: FormGroup;
  submitted: boolean;
  categoryList: any;
  editorConfig = {
    "editable": true,
    "spellcheck": true,
    "height": "50",
    "minHeight": "50",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "Enter text here...",
    "imageEndPoint": "",
    "toolbar": [
        ["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
        ["fontName", "fontSize", "color"],
        ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
        ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
        ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
        ["link", "unlink", "image", "video"]
    ]
};
  constructor(
    private productService: ProductService,
    private _fb: FormBuilder
  ) { }

  // form control name
  get productName() {
    return this.productForm.get('productName');
  }

  ngOnInit() {
    this.getCategories();
    this.buildForm();
  }

  buildForm() {
    this.productForm = this._fb.group({
      category_id: [],
      productName: ['', Validators.required],
      productDetails: ['']
      // discountForBusiness: ['', Validators.]
    })
  }

  getCategories() {
    this.productService.getCategory()
    .subscribe((response) => {
      this.categoryList = response;
    });
  }
  
  submit(value) {
    this.submitted = true;
    console.log(value);
  }
}
