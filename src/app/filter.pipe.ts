import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], nameSearch: string, productCodeSearch: string, statusCode: any){
    if (items && items.length){
        return items.filter(item =>{
            if (nameSearch && item.name.toLowerCase().indexOf(nameSearch.toLowerCase()) === -1){
              return false;
            }
            if (productCodeSearch && item.product_code.toLowerCase().indexOf(productCodeSearch.toLowerCase()) === -1){
              return false;
            }
            if (statusCode && (item.status != parseInt(statusCode, 10))){
              return false;
            }
            return true;
       })
    }
    else{
        return items;
    }
  }  
}
