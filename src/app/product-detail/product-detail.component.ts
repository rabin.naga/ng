import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  productDetail: any;
  constructor(
    private productService: ProductService
  ) { }

  ngOnInit() {
    this.getProductDetail();
  }

  getProductDetail() {
    this.productDetail = this.productService.getProductDetail();
    console.log(this.productDetail);
  }

}
