import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ProductService } from '../product/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit, AfterViewInit {
  title = 'ng6';
  productList: any;
  categoryList: any;
  filteredProductList: any;
  term: string;
  filterByKey: string;
  id: any;
  constructor(
    private productService: ProductService,
    private router: Router
  ) {}

  ngOnInit() {
    this.filterByKey = 'name';
    this.getCatergories();
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts()
    .subscribe((response) => {
      // console.log(index);
      this.productList = response;
      // console.log(response);
      // console.log(this.categoryList);

      this.productList.forEach((product) => {
        this.categoryList.forEach((category) => {
          if (product.category_id === category.category_id) {
            product['category'] = category.title;
          }
        })
      })
      console.log(this.productList);
      this.filteredProductList = this.productList;
    });
  }

  getCatergories() {
    this.productService.getCategory()
    .subscribe((response) => {
      this.categoryList = response;
    });
  }

  filterByCategory(id) {
    this.id = id;
    console.log(id);
    this.filteredProductList = this.productList;
    this.filterProductsByCategoryId(id);
  }

  filterProductsByCategoryId(id) {
    const filteredProducts = this.productList.filter(product => product.category_id == id);
    this.filteredProductList = filteredProducts;
    // console.log(filteredProducts);
  }

  // sorting logic
  key = 'name'; // sort default by name
  reverse = false;
  sortList(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  gotoProductDetail(product) {
    this.router.navigate(['product-detail']);
    this.productService.setProductDetail(product);
    console.log(product);
  }

  getCategoryNameById(id) {
    // return 'abced';
    // if (this.categoryList) {
      this.categoryList.forEach(item => {
        if (item.category_id === id){
          console.log(item.title);
          return item.title;
        }
      })
    // }
  }

  ngAfterViewInit() {
    console.log('after view init')
    // this.getCategoryNameById();
  }

  ngAfterViewChecked() {
    console.log('after view checked');
  }

  ngDoCheck() {
    console.log('ng do checked')
    // this.getProducts();
  }
}
