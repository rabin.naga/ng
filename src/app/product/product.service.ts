import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  product: any;

  constructor(
    private httpClient: HttpClient
  ) { }

  getProducts() {
    return this.httpClient.get(`http://www.kshamadevigroup.com/api/web/v1/products`);
  }

  getCategory() {
    return this.httpClient.get(`http://www.kshamadevigroup.com/api/web/v1/categories`);
  }

  setProductDetail(product) {
    this.product = product;
  }

  getProductDetail() {
    return this.product;
  }
}
